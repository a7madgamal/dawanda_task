import React, { Component } from "react";
import styled from "styled-components";
import InputGroup from "./components/InputGroup.js";
import Button from "./components/Button.js";

const Container = styled.div`
  display: flex;
  align-items: center;
  padding: 1em;
  justify-content: center;
  font-size: 0.7em;
`;

const Wrapper = styled.div`
  width: 95%;
  position: relative;
  max-width: 500px;
`;

const HorizontalSeperator = styled.div`
  display: flex;
  width: 95%;
  text-transform: uppercase;
  margin: 1em auto;

  &:before {
    content: " ";
    border-bottom: 1px solid black;
    flex: 1;
  }
  &:after {
    content: " ";
    border-bottom: 1px solid black;
    flex: 1;
  }
`;

const FormWrapper = styled.form`
  position: relative;
  background-color: white;
  border-radius: 10px;
  padding: 1em;
`;

const Center = styled.div`
  text-align: center;
`;

const noBlank = value => typeof value === "string" && value.length > 0;
const email = value => /[^\s@]+@[^\s@]+\.[^\s@]+/.test(value);
const checked = value => value === true;

const formInputs = [
  {
    key: "vorname",
    label: "vorname",
    type: "text",
    validator: noBlank
  },
  {
    key: "nachname",
    label: "nachname",
    type: "text",
    validator: noBlank
  },
  {
    key: "mitgliedname",
    label: "mitgliedname",
    type: "text",
    validator: noBlank
  },
  {
    key: "email",
    label: "email",
    type: "email",
    validator: email
  },
  {
    key: "passwort",
    label: "passwort",
    type: "password",
    passwordShown: false,
    validator: noBlank
  },
  {
    key: "terms",
    id: "terms",
    label: (
      <span>
        Ich willige in die Verarbeitung und Nutzung meiner Date Geamß der{" "}
        <a href="">Datenschutzerklärung</a> ein.
      </span>
    ),
    type: "checkbox",
    validator: checked
  }
];

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inputs: formInputs.reduce((result, item) => {
        result[item.key] = {
          value: "",
          type: item.type,
          touched: false
        };
        return result;
      }, {})
    };
  }
  handleSubmit = e => {
    e.preventDefault();

    let ready = true;

    const state = { ...this.state };

    Object.keys(state.inputs).forEach(key => {
      state.inputs[key] = {
        ...state.inputs[key],
        ...{
          touched: true
        }
      };
    });

    this.setState(state);

    formInputs.forEach(input => {
      ready = ready && input.validator(this.state.inputs[input.key].value);
    }, this);

    const msg = ready ? "all good. hire me" : "oops, you missed some fields.";
    if (window.speechSynthesis && window.SpeechSynthesisUtterance) {
      speechSynthesis.speak(new SpeechSynthesisUtterance(msg));
    } else {
      alert(msg);
    }
  };

  changeHandler(key, event) {
    const state = { ...this.state };

    if (event.target.type === "checkbox") {
      state.inputs[key].value = event.target.checked;
    } else {
      state.inputs[key].value = event.target.value;
    }

    this.setState(state);
  }

  blurHandler(key, event) {
    const state = { ...this.state };

    state.inputs[key].touched = true;

    this.setState(state);
  }

  passwordCheckboxHandler(key, event) {
    const state = { ...this.state };

    state.inputs[key].passwordShown = event.target.checked;

    this.setState(state);
  }

  render() {
    return (
      <Container>
        <Wrapper>
          <Button theme="facebook" label="Uber Facebook registeren" />
          <HorizontalSeperator>Oder Ohne Facebook</HorizontalSeperator>
          <FormWrapper onSubmit={this.handleSubmit.bind(this)}>
            {formInputs.map(function(input) {
              const { key, label, validator, placeholder } = input;
              const inputState = this.state.inputs[key];

              return (
                <InputGroup
                  key={key}
                  id={key}
                  label={label}
                  isValid={
                    inputState.touched ? validator(inputState.value) : true
                  }
                  placeholder={placeholder}
                  type={inputState.type}
                  value={inputState.value}
                  changeHandler={this.changeHandler.bind(this, key)}
                  blurHandler={this.blurHandler.bind(this, key)}
                  passwordCheckboxHandler={this.passwordCheckboxHandler.bind(
                    this,
                    key
                  )}
                  passwordShown={inputState.passwordShown}
                />
              );
            }, this)}

            <Button type="submit" label="Jetzt registrieren" />
          </FormWrapper>
          <HorizontalSeperator />
          <Center>
            Bereits Mitglied bei DaWanda? <a href="">Hier einloggen</a>
          </Center>
        </Wrapper>
      </Container>
    );
  }
}

export default App;
