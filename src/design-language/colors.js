const colors = {
  lightGray: "#ececec",
  lightOrange: "#fbe0a7",
  darkOrange: "#f1ad55"
};

export default colors;
