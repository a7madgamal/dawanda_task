import React, { Component } from "react";
import styled, { css } from "styled-components";
import dlColors from "../design-language/colors.js";

const InputSection = styled.div`
  transition: margin 0.5s;
  margin-bottom: 1em;
  position: relative;
  ${props =>
    !props.isValid &&
    css`
      margin-bottom: 3em;
    `};
`;

const Label = styled.label`
  display: block;
  text-transform: uppercase;
  color: #6b6b6b;
  padding-bottom: 0.3em;
  font-size: 0.9em;
`;

const InputWrapper = styled.div`
  display: flex;
  flex-direction: row;
`;

const Icon = styled.div`
  width: 40px;
  border-radius: 7px 0 0 7px;
  border: 1px solid #ddd;
  border-right: none;
  margin: 0;
  background-color: ${dlColors.lightGray};
  background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAfBJREFUWAntVk1OwkAUZkoDKza4Utm61iP0AqyIDXahN2BjwiHYGU+gizap4QDuegWN7lyCbMSlCQjU7yO0TOlAi6GwgJc0fT/fzPfmzet0crmD7HsFBAvQbrcrw+Gw5fu+AfOYvgylJ4TwCoVCs1ardYTruqfj8fgV5OUMSVVT93VdP9dAzpVvm5wJHZFbg2LQ2pEYOlZ/oiDvwNcsFoseY4PBwMCrhaeCJyKWZU37KOJcYdi27QdhcuuBIb073BvTNL8ln4NeeR6NRi/wxZKQcGurQs5oNhqLshzVTMBewW/LMU3TTNlO0ieTiStjYhUIyi6DAp0xbEdgTt+LE0aCKQw24U4llsCs4ZRJrYopB6RwqnpA1YQ5NGFZ1YQ41Z5S8IQQdP5laEBRJcD4Vj5DEsW2gE6s6g3d/YP/g+BDnT7GNi2qCjTwGd6riBzHaaCEd3Js01vwCPIbmWBRx1nwAN/1ov+/drgFWIlfKpVukyYihtgkXNp4mABK+1GtVr+SBhJDbBIubVw+Cd/TDgKO2DPiN3YUo6y/nDCNEIsqTKH1en2tcwA9FKEItyDi3aIh8Gl1sRrVnSDzNFDJT1bAy5xpOYGn5fP5JuL95ZjMIn1ya7j5dPGfv0A5eAnpZUY3n5jXcoec5J67D9q+VuAPM47D3XaSeL4AAAAASUVORK5CYII=");
  background-repeat: no-repeat;
  background-size: 16px 18px;
  background-position: 50% 50%;
`;

const StyledInput = styled.input`
  flex-grow: 1;
  border-radius: 0 5px 5px 0;
  border: 1px solid #ddd;
  transition: border-color 0.5s;
  font-size: 1em;
  padding: 1em;
  outline: none;

  ${props =>
    !props.isValid &&
    css`
      border-color: ${dlColors.darkOrange};
    `};

  ${props =>
    props.type === "password" &&
    css`
      border-radius: 0;
    `};
`;

const ValidationMessage = styled.span`
  background-color: ${dlColors.lightOrange};
  color: ${dlColors.darkOrange};
  padding: 0.4em;
  position: absolute;
  left: 3em;
  top: 5em;
  z-index: 2;
  opacity: 0;
  transition: opacity 0.5s;
  &:after {
    content: "";
    position: absolute;
    width: 10px;
    height: 10px;
    left: 10px;
    top: -4px;
    transform: rotate(45deg);
    background-color: ${dlColors.lightOrange};
    z-index: 1;
  }

  ${props =>
    !props.isValid &&
    css`
      opacity: 1;
    `};
`;

const SubInputDivider = styled.div`
  display: flex;
  border-radius: 0 5px 5px 0;
  background-color: ${dlColors.lightGray};
`;

const SubInputWrapper = styled.div`
  padding: 0.5em;
  margin: auto 0;
`;

const SubInput = styled.input`
  display: inline-block;
`;
const SubInputLabel = styled.label`
  margin-left: 0.5em
  display: inline-block;
`;

const Checkbox = styled.input`
  float: left;

  ${props =>
    !props.isValid &&
    css`
      outline: 2px solid ${dlColors.lightOrange};
    `};
`;

const CheckboxLabel = styled.label`
  color: #6b6b6b;
  display: inline-block;
  width: 90%;
`;

const CheckboxGroup = styled.div`
  padding: 0.5em;
`;

class Input extends Component {
  render() {
    const {
      id,
      label,
      isValid,
      placeholder,
      type,
      value,
      passwordShown,
      changeHandler,
      blurHandler,
      passwordCheckboxHandler
    } = this.props;

    let inputType = type || "text";
    inputType = inputType === "password" && passwordShown ? "text" : inputType;

    const isCheckbox = type === "checkbox";

    return isCheckbox ? (
      <CheckboxGroup>
        <Checkbox
          id={id}
          type={inputType}
          onChange={changeHandler}
          isValid={isValid}
        />
        <CheckboxLabel htmlFor={id}>{label}</CheckboxLabel>
      </CheckboxGroup>
    ) : (
      <InputSection type={type} isValid={isValid}>
        <Label htmlFor="">{label}</Label>
        <InputWrapper>
          <Icon />

          <StyledInput
            value={value}
            type={inputType}
            placeholder={placeholder}
            onChange={changeHandler}
            onBlur={blurHandler}
            isValid={isValid}
          />

          {type === "password" && (
            <SubInputDivider>
              <SubInputWrapper>
                <SubInput type="checkbox" onChange={passwordCheckboxHandler} />
                <SubInputLabel>Anzeigen</SubInputLabel>
              </SubInputWrapper>
            </SubInputDivider>
          )}

          <ValidationMessage isValid={isValid}>
            {type === "email" ? "muss email werden" : "muss ausgefüllt werden"}
          </ValidationMessage>
        </InputWrapper>
      </InputSection>
    );
  }
}

export default Input;
