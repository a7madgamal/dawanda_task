import React from "react";
import styled from "styled-components";
import renderer from "react-test-renderer";
import "jest-styled-components";
import Button from "./Button";

test("green by default", () => {
  const tree = renderer.create(<Button />).toJSON();
  expect(tree).toMatchSnapshot();
  expect(tree).toHaveStyleRule("background-color", "#719d00");
});

test("...but blue for facebook", () => {
  const tree = renderer.create(<Button theme="facebook" />).toJSON();
  expect(tree).toMatchSnapshot();
  expect(tree).toHaveStyleRule("background-color", "#2f5a9c");
});
