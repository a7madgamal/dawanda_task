import React, { Component } from "react";
import styled, { css } from "styled-components";

const StyledButton = styled.button`
  width: 100%;
  color: white;
  border-radius: 0.2em;
  padding: 0.7em;
  text-align: center;
  cursor: pointer;
  outline: none;
  margin: 0 auto;
  display: block;
  background-color: #719d00;
  position: relative;
  ${props =>
    props.theme === "facebook" &&
    css`
      background-color: #2f5a9c;
      width: 95%;
      box-shadow: 1px 1px 1px 1px #4b5d7a;
      border: none;
    `};
`;

const ButtonIcon = styled.span`
  position: absolute;
  left: 10px;
  height: 18px;
  width: 20px;

  ${props =>
    props.theme === "facebook" &&
    css`
      background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAAXNSR0IArs4c6QAAAfBJREFUWAntVk1OwkAUZkoDKza4Utm61iP0AqyIDXahN2BjwiHYGU+gizap4QDuegWN7lyCbMSlCQjU7yO0TOlAi6GwgJc0fT/fzPfmzet0crmD7HsFBAvQbrcrw+Gw5fu+AfOYvgylJ4TwCoVCs1ardYTruqfj8fgV5OUMSVVT93VdP9dAzpVvm5wJHZFbg2LQ2pEYOlZ/oiDvwNcsFoseY4PBwMCrhaeCJyKWZU37KOJcYdi27QdhcuuBIb073BvTNL8ln4NeeR6NRi/wxZKQcGurQs5oNhqLshzVTMBewW/LMU3TTNlO0ieTiStjYhUIyi6DAp0xbEdgTt+LE0aCKQw24U4llsCs4ZRJrYopB6RwqnpA1YQ5NGFZ1YQ41Z5S8IQQdP5laEBRJcD4Vj5DEsW2gE6s6g3d/YP/g+BDnT7GNi2qCjTwGd6riBzHaaCEd3Js01vwCPIbmWBRx1nwAN/1ov+/drgFWIlfKpVukyYihtgkXNp4mABK+1GtVr+SBhJDbBIubVw+Cd/TDgKO2DPiN3YUo6y/nDCNEIsqTKH1en2tcwA9FKEItyDi3aIh8Gl1sRrVnSDzNFDJT1bAy5xpOYGn5fP5JuL95ZjMIn1ya7j5dPGfv0A5eAnpZUY3n5jXcoec5J67D9q+VuAPM47D3XaSeL4AAAAASUVORK5CYII=");
      background-repeat: no-repeat;
      background-size: 16px 18px;
      background-position: 50% 50%;
    `};
`;

class Button extends Component {
  render() {
    const { theme, label } = this.props;

    return (
      <StyledButton theme={theme}>
        <ButtonIcon theme={theme} />
        {label}
      </StyledButton>
    );
  }
}

export default Button;
